//
//  LoginViewController.swift
//  swiftBookNetworking
//
//  Created by Artem Chursin on 12.11.2019.
//  Copyright © 2019 Artem Chursin. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FirebaseAuth
import FirebaseDatabase
import GoogleSignIn

class LoginViewController: UIViewController {
    //MARK: - Constants
    
    //MARK: - Variables
    lazy var fbLoginButton: UIButton = {
        
        let loginButton = FBLoginButton()
        loginButton.delegate = self
        loginButton.frame = CGRect(x: 32, y: 360, width: view.frame.width - 64, height: 50)
        return loginButton
    }()
    
    lazy var customFBLoginButton: UIButton = {
       
        let button = UIButton()
        button.backgroundColor = .lightGray
        button.setTitle("Login with facebook", for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        button.setTitleColor(.red, for: .normal)
        button.frame = CGRect(x: 32, y: 360 + 60, width: view.frame.width - 64, height: 50)
        button.layer.cornerRadius = 4
        button.addTarget(self, action: #selector(handleCustomFBLogin), for: .touchUpInside)
        
        
        return button
    }()
    
    lazy var googleLoginButton: GIDSignInButton = {
       
        let loginButton = GIDSignInButton()
        loginButton.frame = CGRect(x: 32, y: 360 + 80 + 80, width: view.frame.width - 64, height: 50)
        return loginButton
    }()
    
    lazy var googleCustomLoginButton: UIButton = {
       
        let loginButton = UIButton()
        loginButton.frame = CGRect(x: 32, y: 360 + 80 + 80 + 80, width: view.frame.width - 64, height: 50)
        loginButton.backgroundColor = .green
        loginButton.setTitle("Login with google", for: .normal)
        loginButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        loginButton.setTitleColor(.white, for: .normal)
        loginButton.layer.cornerRadius = 4
        loginButton.addTarget(self, action: #selector(handleCustomGoogleLogin), for: .touchUpInside)
        return loginButton
    }()
    
    lazy var signInWithEmail: UIButton = {
        
        let loginButton = UIButton()
        loginButton.frame = CGRect(x: 32, y: 360 + 80 + 80 + 80 + 80, width: view.frame.width - 64, height: 50)
        loginButton.setTitle("Sign In with Email", for: .normal)
        loginButton.addTarget(self, action: #selector(openSignInVC), for: .touchUpInside)
        return loginButton
    }()
    
    var userProfile: UserProfile?
    
    //MARK: - Outlets
    
    //MARK: - LifeStyle ViewController
    override func viewDidLoad() {
        super.viewDidLoad()

        view.addVerticalGradientLayer(topColor: primaryColor, bottomColor: secondaryColor)
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance()?.presentingViewController = self
        setupViews()

    }
    
    //MARK: - Private methods
    private func setupViews() {
        view.addSubview(fbLoginButton)
        view.addSubview(customFBLoginButton)
        view.addSubview(googleLoginButton)
        view.addSubview(googleCustomLoginButton)
        view.addSubview(signInWithEmail)
    }
    
    @objc private func openSignInVC() {
        performSegue(withIdentifier: "SignIn", sender: self)
    }

}
//MARK: - Facebook
extension  LoginViewController: LoginButtonDelegate {
    
    func loginButton(_ loginButton: FBLoginButton, didCompleteWith result: LoginManagerLoginResult?, error: Error?) {
        
        if error != nil {
            print(error!)
            return
        }
        guard AccessToken.isCurrentAccessTokenActive else {
            return
        }
        
        
        print("Successfully logged in with facebook ...")
        self.signIntoFirebase()
    }
    
    private func openMainViewController() {
        
        dismiss(animated: true, completion: nil)
    }
    
    func loginButtonDidLogOut(_ loginButton: FBLoginButton) {
        
        print("Did logout facebook")
    }
    
    @objc private func handleCustomFBLogin() {
        
        LoginManager().logIn(permissions: ["email", "public_profile"], from: self) { (result, error) in
            
            if let error = error {
                print(error.localizedDescription)
                return
            }
            
            guard let result = result else {
                return
            }
            
            if result.isCancelled {
                return
            } else {
                self.signIntoFirebase()
            }
        }
    }
    
    private func signIntoFirebase() {
        let accessToken = AccessToken.current
        guard let accessTokenString = accessToken?.tokenString else {
            return
        }
        
        let credentials = FacebookAuthProvider.credential(withAccessToken: accessTokenString)
        Auth.auth().signIn(with: credentials) { (user, error) in
            
            if let error = error {
                print("Error with facebook auth in firebace", error)
                return
            }
            
            print("Successfully logged in with our FB user")
            self.fetchFacebookFields()
        }
    }
    
    private func fetchFacebookFields() {
        
        GraphRequest(graphPath: "me", parameters: ["fields":"id, name, email"]).start { (_, result, error) in
            
            if let error = error {
                print(error)
                return
            }
            
            if let userData = result as? [String: Any] {
                
                self.userProfile = UserProfile(data: userData)
                print(self.userProfile?.name ?? "nil")
                print(userData)
                self.saveIntoFirebase()
            }
        }
    }
    
    private func saveIntoFirebase() {
        
        guard  let uid = Auth.auth().currentUser?.uid else {
            return
        }
        
        let userData = ["name": userProfile?.name, "email": userProfile?.email]
        
        let values = [uid: userData]
        
        Database.database().reference().child("users").updateChildValues(values) { (error, _) in
            
            if let error = error {
                print(error)
                return
            }
            print("Success save into firebase!")
            self.openMainViewController()
        }
    }
}

//MARK: - GOOGLE SDK
extension LoginViewController: GIDSignInDelegate {
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if let error = error {
            print("failed log into google", error)
            return
        }
        print("Successfully logged into google")
        
        if let userName = user.profile.name, let userEmail = user.profile.email {
            
            let userData = ["name": userName, "email": userEmail]
            userProfile = UserProfile(data: userData)
        }
        
        guard let authentication = user.authentication else {
            return
        }
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                       accessToken: authentication.accessToken)
        
        Auth.auth().signIn(with: credential) { (user, error) in
            
            if let error = error {
                
                print("Something went wrong with our Google user: ", error)
                return
            }
            
            print("Successfully logged into Firebace in google")
            self.saveIntoFirebase()
        }
    }
    
    @objc private func handleCustomGoogleLogin() {
        
        GIDSignIn.sharedInstance()?.signIn()
    }
}
