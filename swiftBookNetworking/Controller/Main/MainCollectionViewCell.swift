//
//  MainCollectionViewCell.swift
//  swiftBookNetworking
//
//  Created by Artem Chursin on 05.11.2019.
//  Copyright © 2019 Artem Chursin. All rights reserved.
//

import UIKit


class MainCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
}
