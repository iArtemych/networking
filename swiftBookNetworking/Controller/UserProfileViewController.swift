//
//  userProfileViewController.swift
//  swiftBookNetworking
//
//  Created by Artem Chursin on 12.11.2019.
//  Copyright © 2019 Artem Chursin. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FirebaseAuth
import FirebaseDatabase
import GoogleSignIn

class UserProfileViewController: UIViewController {

    //MARK: - Variables
    private var provider: String?
    private var currentUser: CurrentUser?
    lazy var logoutButton: UIButton = {
        
        let button = UIButton()
        
        button.frame = CGRect(x: 32,
                                   y: view.frame.height - 172,
                                   width: view.frame.width - 64,
                                   height: 50)
        button.backgroundColor = UIColor.blue
        button.setTitle("Logout", for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        button.setTitleColor(.systemBackground, for: .normal)
        button.layer.cornerRadius = 4
        button.addTarget(self, action: #selector(signOut), for: .touchUpInside)
        return button
    }()
    
    //MARK: - Outlets
    @IBOutlet var loggedLabel: UILabel!
    @IBOutlet weak var logginActivity: UIActivityIndicatorView!
    
    //MARK: - LifeStyle ViewController
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        fetchingUserData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addVerticalGradientLayer(topColor: primaryColor, bottomColor: secondaryColor)
        loggedLabel.isHidden = true

        setupViews()
    }
    
    //MARK: - Private methods
    private func setupViews() {
        view.addSubview(logoutButton)
    }

}

//MARK: - SDK
extension  UserProfileViewController {
    
    private func openLoginViewController() {
        
        do {
            try Auth.auth().signOut()
            
            DispatchQueue.main.async {
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let loginViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                loginViewController.modalPresentationStyle = .fullScreen
                self.present(loginViewController, animated: true, completion: nil)
                return
            }
        } catch let error {
            print("failed to sign out with error: ", error.localizedDescription)
        }
    }
    
    private func fetchingUserData() {
        
        if Auth.auth().currentUser != nil {
            
            if let userName = Auth.auth().currentUser?.displayName {
                logginActivity.stopAnimating()
                loggedLabel.isHidden = false
                loggedLabel.text = getProviderData(with: userName)
            } else {
                
                guard let uid = Auth.auth().currentUser?.uid else {
                    return
                }
                
                Database.database().reference().child("users").child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
                    guard let userData = snapshot.value as? [String: Any] else {
                        return
                    }
                    self.currentUser = CurrentUser(uid: uid, data: userData)
                    
                    self.logginActivity.stopAnimating()
                    self.loggedLabel.isHidden = false
                    self.loggedLabel.text = self.getProviderData(with: self.currentUser?.name ?? "Noname")
                    
                }) { (error) in
                    print(error)
                }
            }
        }
    }
    
    @objc private func signOut() {
        
        if let providerData = Auth.auth().currentUser?.providerData {
            
            for userInfo in providerData {
                
                switch userInfo.providerID {
                case "facebook.com":
                    LoginManager().logOut()
                    print("User did log out of facebook")
                    openLoginViewController()
                case "google.com":
                    GIDSignIn.sharedInstance()?.signOut()
                    print("User did log out of google")
                    openLoginViewController()
                case "password":
                    try! Auth.auth().signOut()
                    print("User did log out of password")
                    openLoginViewController()
                default:
                    print("User is signed in with \(userInfo.providerID)")
                }
            }
        }
    }
    
    private func getProviderData(with user: String) -> String {
        
        var greetings = ""
        
        if let providerData = Auth.auth().currentUser?.providerData {
            
            for userInfo in providerData {
                
                switch userInfo.providerID {
                case "facebook.com":
                    provider = "Facebook"
                case "google.com":
                    provider = "Google"
                case "password":
                    provider = "Email"
                default:
                    break
                }
            }
            guard let provider = provider else {
                return "Error with checl login"
            }
            greetings = "\(user) Logged in with \(provider)"
        }
        return greetings
    }
}
