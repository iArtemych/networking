//
//  CourseTableViewCell.swift
//  swiftBookNetworking
//
//  Created by Artem Chursin on 05.11.2019.
//  Copyright © 2019 Artem Chursin. All rights reserved.
//

import UIKit

class CourseTableViewCell: UITableViewCell {

    
    @IBOutlet weak var imageCourse: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var numberOfLessons: UILabel!
    @IBOutlet weak var numberOfTests: UILabel!
    

    func configCell(course: Course) {
        
        name.text = course.name
        
        if let numberOfLessons = course.numberOfLessons {
            
            self.numberOfLessons.text = "Number of lessons: \(numberOfLessons)"
        }
        
        if let numberOfTests = course.numberOfTests {
            
            self.numberOfTests.text = "Number of tests: \(numberOfTests)"
        }
        
        DispatchQueue.global().async {
            guard let url = URL(string: course.imageUrl!) else {
                return
            }
            guard let imageData = try? Data(contentsOf: url) else {
                return
            }
            
            DispatchQueue.main.async {
                self.imageCourse.image = UIImage(data: imageData)
            }
        }
    }
}
