//
//  WebViewController.swift
//  swiftBookNetworking
//
//  Created by Artem Chursin on 05.11.2019.
//  Copyright © 2019 Artem Chursin. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController {
    
    //MARK: - Constants
    
    //MARK: - Variables
    var selectedCourse: String?
    var courseURL = ""
    
    //MARK: - Outlets
    @IBOutlet weak var detailWebView: WKWebView!
    @IBOutlet weak var progressView: UIProgressView!
    
    //MARK: - LifeStyle ViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = selectedCourse
        
        guard let url = URL(string: courseURL) else {
            return
        }
        let request = URLRequest(url: url)
        
        detailWebView.load(request)
        detailWebView.allowsBackForwardNavigationGestures = true
        detailWebView.navigationDelegate = self
        detailWebView.addObserver(self,
                                  forKeyPath: #keyPath(WKWebView.estimatedProgress),
                                  options: .new,
                                  context: nil)
        
    }
    
    override func observeValue(forKeyPath keyPath: String?,
                               of object: Any?,
                               change: [NSKeyValueChangeKey : Any]?,
                               context: UnsafeMutableRawPointer?) {
        
        if keyPath == "estimatedProgress" {
            progressView.progress = Float(detailWebView.estimatedProgress)
        }
    }
    
    //MARK: - Actions
    
    //MARK: - Navigation
    
    //MARK: - Private methods
    
    private func showProgressView() {
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {
            self.progressView.alpha = 1
        }, completion: nil)
    }
    
    private func hideProgressView() {
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {
            self.progressView.alpha = 0
        }, completion: nil)
    }

}

extension WebViewController: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        showProgressView()
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        hideProgressView()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        hideProgressView()
    }
}
