//
//  MainViewController.swift
//  swiftBookNetworking
//
//  Created by Artem Chursin on 05.11.2019.
//  Copyright © 2019 Artem Chursin. All rights reserved.
//

import UIKit
import UserNotifications
import FBSDKLoginKit
import FirebaseAuth

enum Actions: String, CaseIterable {
    case DownloadImage = "Download image"
    case Get = "GET"
    case Post = "POST"
    case OurCourses = "Our Courses"
    case UploadImage = "Upload image"
    case DownloadFile = "Download File"
    case OurCoursesAlamofire = "Our Courses (Alamofire)"
    case ResponseDataAlamofire = "Response Data Alamofire"
    case ResponseString = "Response String"
    case Response = "Response"
    case DownloadLargeImage = "Download Large Image"
    case PostAlamofire = "Post with Alamofire"
    case PutRequest = "Put Request Alamofire"
    case UploadImageAlamofire = "Upload Image Alamofire"
}

private let url = "https://jsonplaceholder.typicode.com/posts"
private let swiftBookUrl = "https://swiftbook.ru//wp-content/uploads/api/api_courses"

class MainViewController: UIViewController {
    
    //MARK: - Constants
    let actions = Actions.allCases
    private let dataProvider = DataProvider()
    //MARK: - Variables
    private var alert: UIAlertController!
    private var filePath: String?
    
    //MARK: - Outlets
    @IBOutlet weak var mainCollectionView: UICollectionView!
    
    //MARK: - LifeStyle ViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        
        checkLoggedIn()
        registerForNotification()
        
        dataProvider.fileLocation = { (location) in
            // save file
            print("Download finish: \(location.absoluteString)")
            self.filePath = location.absoluteString
            self.alert.dismiss(animated: false, completion: nil)
            self.postNotification()
        }
    }
    
    //MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let coursesVC = segue.destination as? CoursesViewController
        let imageVC = segue.destination as? ImageViewController
        
        switch segue.identifier {
        case "toCourses":
            coursesVC?.fetchData()
        case "toCoursesAlamofire":
            coursesVC?.fetchDataWithAlamofire()
        case "toDownloadImage":
            imageVC?.getImage()
        case "toDownloadImageAlamofire":
            imageVC?.getImageAlamofire()
        case "toBigData":
            imageVC?.getImageWithProgress()
        case "postAlamofire":
            coursesVC?.postAlamofire()
        case "putRequest":
            coursesVC?.putRequest()
        default:
            break
        }
    }
    //MARK: - Private methods
    private func showAlert() {
        
        alert = UIAlertController(title: "Downloading...", message: "0%", preferredStyle: .alert)
        let height = NSLayoutConstraint(item: alert.view!,
                                       attribute: .height,
                                       relatedBy: .equal,
                                       toItem: nil,
                                       attribute: .notAnAttribute,
                                       multiplier: 0,
                                       constant: 170)
        
        alert.view.addConstraint(height)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive) { (action) in
            
            self.dataProvider.stopDownload()
        }
        alert.addAction(cancelAction)
        present(alert, animated: true) {
            
            let size = CGSize(width: 40, height: 40)
            let point = CGPoint(x: self.alert.view.frame.width / 2 - size.width / 2, y: self.alert.view.frame.height / 2 - size.height / 2)
            
            let activityIndicator = UIActivityIndicatorView(frame: CGRect(origin: point, size: size))
            activityIndicator.color = .gray
            activityIndicator.startAnimating()
            
            let progressView = UIProgressView(frame: CGRect(x: 0, y: self.alert.view.frame.height - 44, width: self.alert.view.frame.width, height: 2))
            progressView.tintColor = .blue
            self.dataProvider.onProgress = { (progress) in
                
                progressView.progress = Float(progress)
                self.alert.message = String(Int(progress * 100)) + "%"
            }
            
            self.alert.view.addSubview(activityIndicator)
            self.alert.view.addSubview(progressView)
            
        }
    }
}

extension MainViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        actions.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Task", for: indexPath) as! MainCollectionViewCell
        
        cell.titleLabel.text = actions[indexPath.row].rawValue
        
        return cell
    }
    
    
}

extension MainViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let action = actions[indexPath.row]
        
        switch action {
        case .DownloadImage:
            performSegue(withIdentifier: "toDownloadImage", sender: self)
        case .Get:
            NetworkManager.getRequest(url: url)
        case .Post:
            NetworkManager.postRequest(url: url)
        case .OurCourses:
            performSegue(withIdentifier: "toCourses", sender: self)
        case .UploadImage:
            print("Upload")
        case .DownloadFile:
            showAlert()
            dataProvider.startDownload()
        case .OurCoursesAlamofire:
            performSegue(withIdentifier: "toCoursesAlamofire", sender: self)
        case .ResponseDataAlamofire:
            performSegue(withIdentifier: "toDownloadImageAlamofire", sender: self)
            AlamofireNetworkManager.responseData(url: swiftBookUrl)
        case .ResponseString:
            AlamofireNetworkManager.responseString(url: swiftBookUrl)
        case .Response:
            AlamofireNetworkManager.response(url: swiftBookUrl)
        case.DownloadLargeImage:
            performSegue(withIdentifier: "toBigData", sender: self)
        case .PostAlamofire:
            performSegue(withIdentifier: "postAlamofire", sender: self)
        case .PutRequest:
            performSegue(withIdentifier: "putRequest", sender: self)
        case .UploadImageAlamofire:
            print("HUY")
        }
        
        
        
        
    }
}

extension MainViewController {
    
    private func registerForNotification() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound]) { (_, _) in
            
        }
    }
    
    private func postNotification() {
        
        let content = UNMutableNotificationContent()
        content.title = "Download complete!"
        content.body = "Your background transfer has completes. File path \(filePath!)"
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 3, repeats: false)
        
        let request = UNNotificationRequest(identifier: "TransferComplete", content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
}
// MARK: - Facebook SDK
extension MainViewController {
    
    private func checkLoggedIn() {
        
        if Auth.auth().currentUser == nil {
            
            DispatchQueue.main.async {
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let loginViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                loginViewController.modalPresentationStyle = .fullScreen
                self.present(loginViewController, animated: true, completion: nil)
                return
            }
        }
    }
}
