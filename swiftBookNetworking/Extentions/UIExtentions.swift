//
//  UIExtentions.swift
//  swiftBookNetworking
//
//  Created by Artem Chursin on 12.11.2019.
//  Copyright © 2019 Artem Chursin. All rights reserved.
//

import UIKit

extension UIView {
    
    func addVerticalGradientLayer(topColor: UIColor, bottomColor: UIColor ) {
        
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = [topColor.cgColor, bottomColor.cgColor]
        gradient.locations = [0.0, 1.0]
        gradient.startPoint = CGPoint(x: 0, y: 0)
        gradient.endPoint = CGPoint(x: 0, y: 1)
        self.layer.insertSublayer(gradient, at: 0)
    }
}
