//
//  AlamofireNetworkManager.swift
//  swiftBookNetworking
//
//  Created by Artem Chursin on 11.11.2019.
//  Copyright © 2019 Artem Chursin. All rights reserved.
//

import Foundation
import Alamofire

class AlamofireNetworkManager {
    
    static var onProgress: ((Double) -> ())?
    static var complited: ((String) -> ())?
    
    static func sendRequest(url: String, completion: @escaping (_ courses: [Course])->()) {
        
        guard let url = URL(string: url) else {
            return
        }
        
        request(url, method: .get).validate().responseJSON { (response) in
            
            switch response.result {
            case .success(let value):
                
                var courses: [Course] = []
                courses = Course.getArray(from: value)!
                completion(courses)
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    static func responseData(url: String) {
        
        request(url).responseData { (responseData) in
            
            switch responseData.result {
            case .success(let data):
                guard let string = String(data: data, encoding: .utf8) else {
                    return
                }
                print(string)
            case .failure(let error):
                print(error)
            }
        }
    }
    
    static func responseString(url: String) {
        
        request(url).responseString { (responseString) in
            
            switch responseString.result {
            case .success(let stringResult):
                print(stringResult)
            case .failure(let error):
                print(error)
            }
        }
    }
    
    static func response(url: String) {
        
        request(url).response { (response) in
            
            guard let data = response.data, let string = String(data: data, encoding: .utf8) else {
                    return
            }
            print(string)
        }
    }
    
    static func getAlamofireImage(url: String, completion: @escaping (_ image: UIImage)->()) {
        
        guard let url = URL(string: url) else {
            return
        }
        
        request(url).responseData { (responseData) in
            
            switch responseData.result {
            case .success(let data):
                guard let image = UIImage(data: data) else {
                    return
                }
                completion(image)
            
            case .failure(let error):
                print(error)
            }
        }
    }
    
    static func downloadProgress(url: String, completion: @escaping (_ image: UIImage)->()) {
        
        guard let url = URL(string: url) else {
            return
        }
        
        request(url).validate().downloadProgress { (progress) in
            
            print("Total unit count \n", progress.totalUnitCount)
            print("Complited Unit count \n", progress.completedUnitCount)
            print("Fractioncompleted \n", progress.fractionCompleted)
            print("Loclized description \n", progress.localizedDescription!)
            print("-------------------------------------------------")
            
            self.onProgress?(progress.fractionCompleted)
            self.complited?(progress.localizedDescription)
        }.response { (response) in
            
            guard let data = response.data, let image = UIImage(data: data) else {
                return
            }
            DispatchQueue.main.async {
                completion(image)
            }
        }
    }
    
    static func postRequesr(url: String, completion: @escaping (_ courses: [Course])->()) {
        
        guard let url = URL(string: url) else {
            return
        }
        
        let userData: [String: Any] = ["name": "Network Request",
                                       "link": "https://swiftbook.ru/contents/our-first-applications/",
                                       "imageUrl": "https://swiftbook.ru/wp-content/uploads/2018/03/2-courselogo.jpg",
                                       "numberOfLessons": 20,
                                       "numberOfTests": 10]
        
        request(url, method: .post, parameters: userData).responseJSON { (responseJSON) in
            
            guard let statusCode = responseJSON.response?.statusCode else {
                return
            }
            print("statusCode", statusCode)
            
            switch responseJSON.result {
            case .success(let value):
                print(value)
                
                guard let jsonObject = value as? [String: Any], let course = Course(json: jsonObject) else {
                    return
                }
                var courses: [Course] = []
                courses.append(course)
                
                completion(courses)
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    static func putRequesr(url: String, completion: @escaping (_ courses: [Course])->()) {
        
        guard let url = URL(string: url) else {
            return
        }
        
        let userData: [String: Any] = ["name": "Network Request with alamofire",
                                       "link": "https://swiftbook.ru/contents/our-first-applications/",
                                       "imageUrl": "https://swiftbook.ru/wp-content/uploads/2018/03/2-courselogo.jpg",
                                       "numberOfLessons": 20,
                                       "numberOfTests": 10]
        
        request(url, method: .put, parameters: userData).responseJSON { (responseJSON) in
            
            guard let statusCode = responseJSON.response?.statusCode else {
                return
            }
            print("statusCode", statusCode)
            
            switch responseJSON.result {
            case .success(let value):
                print(value)
                
                guard let jsonObject = value as? [String: Any], let course = Course(json: jsonObject) else {
                    return
                }
                var courses: [Course] = []
                courses.append(course)
                
                completion(courses)
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    static func uploadImage(url: String) {
        
        guard let url = URL(string: url) else {
            return
        }
        
        let image = UIImage(named: "Lake")!
        let data = image.pngData()!
        
        let httpHeaders = ["Authorization":"_______"]
        
        upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(data, withName: "image")
        }, to: url, headers: httpHeaders) { (encodingCompletion) in
            
            switch encodingCompletion {
            case .success(request: let uploadRequest,
                          streamingFromDisk: let streamingFromDisk,
                          streamFileURL: let streamFileURL):
                print(uploadRequest)
                print(streamingFromDisk)
                print(streamFileURL ?? "streamFileURL is nill")
                
                uploadRequest.validate().responseJSON { (responseJSON) in
                    
                    switch responseJSON.result {
                    case .success(let value):
                        print(value)
                    case .failure(let error):
                        print(error)
                    }
                    
                }
            case .failure(let error):
                print(error)
            }
        }
    }
}
