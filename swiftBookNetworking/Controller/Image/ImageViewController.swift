//
//  ViewController.swift
//  swiftBookNetworking
//
//  Created by Artem Chursin on 04.11.2019.
//  Copyright © 2019 Artem Chursin. All rights reserved.
//

import UIKit
import Alamofire

class ImageViewController: UIViewController {
    
    private let url = "https://storage.yandexcloud.net/aip1/walp-ios13%2FmacOS-walp%2FLight%20mode%20JPEG.jpg"

//MARK: - Constants

//MARK: - Variables

//MARK: - Outlets
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var activIndicator: UIActivityIndicatorView!
    @IBOutlet weak var completedLabel: UILabel!
    @IBOutlet weak var downloadProgress: UIProgressView!
    
//MARK: - LifeStyle ViewController
    override func viewDidLoad() {
        super.viewDidLoad()
     
        activIndicator.startAnimating()
        activIndicator.hidesWhenStopped = true
        downloadProgress.isHidden = true
        completedLabel.isHidden = true
    }
    
    //MARK: - Actions
    func getImage() {
        
        NetworkManager.downloadImage(url: url) { (image) in
            
            self.activIndicator.stopAnimating()
            self.imageView.image = image
        }
    }
    
    func getImageAlamofire(){
        
        AlamofireNetworkManager.getAlamofireImage(url: url) { (image) in
            
            self.activIndicator.stopAnimating()
            self.imageView.image = image
        }
    }
    
    func getImageWithProgress() {
        
        AlamofireNetworkManager.onProgress = { progress in
            
            self.downloadProgress.isHidden = false
            self.downloadProgress.progress = Float(progress)
        }
        
        AlamofireNetworkManager.complited = { completed in
            
            self.completedLabel.isHidden = false
            self.completedLabel.text = completed
        }
        
        AlamofireNetworkManager.downloadProgress(url: url) { (image) in
            
            self.activIndicator.stopAnimating()
            self.completedLabel.isHidden = true
            self.downloadProgress.isHidden = true
            self.imageView.image = image
            
        }
    }
}

