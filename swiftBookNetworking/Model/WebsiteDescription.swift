//
//  WebsiteDescription.swift
//  swiftBookNetworking
//
//  Created by Artem Chursin on 05.11.2019.
//  Copyright © 2019 Artem Chursin. All rights reserved.
//

import Foundation

struct WebsiteDescription: Decodable {
    
    let websiteDescription: String?
    let websiteName: String?
    let courses: [Course]
}
