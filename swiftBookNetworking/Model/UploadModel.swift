//
//  UploadModel.swift
//  swiftBookNetworking
//
//  Created by Artem Chursin on 06.11.2019.
//  Copyright © 2019 Artem Chursin. All rights reserved.
//

import Foundation
import UIKit

struct UploadImage {
    
    let key: String
    let data: Data
    
    init?(withImage image: UIImage, forKey key: String) {
        
        self.key = key
        guard let data = image.pngData() else {
            return nil
        }
        self.data = data
    }
}
