//
//  NetworkManager.swift
//  swiftBookNetworking
//
//  Created by Artem Chursin on 05.11.2019.
//  Copyright © 2019 Artem Chursin. All rights reserved.
//

import Foundation
import UIKit

class NetworkManager {
    
    static func downloadImage(url: String, complition: @escaping (_ image: UIImage)->()) {
        
        guard let url = URL(string: url) else {
            return
        }
        
        let session = URLSession.shared
        session.dataTask(with: url) { (data, response, error) in
            if let data = data, let image = UIImage(data: data) {
                DispatchQueue.main.async {
                    complition(image)
                }
            }
        }.resume()
    }
    
    static func getRequest(url: String) {

        guard let url = URL(string: url) else {
            return
        }
        
        let session = URLSession.shared
        session.dataTask(with: url) { (data, response, error) in

            guard let response = response, let data = data else {
                return
            }
            print(response)
            print("---------------")
            print(data)

            do {
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                print(json)

            } catch {
                print(error)
            }

        }.resume()
    }

    static func postRequest(url: String) {

        guard let url = URL(string: url) else {
            return
        }
        let userData = ["Course": "Networking", "Lesson": "GET and POST"]

        var request = URLRequest(url: url)
        request.httpMethod = "POST"

        guard let httpBody = try? JSONSerialization.data(withJSONObject: userData, options: []) else {
            return
        }
        request.httpBody = httpBody
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in

            guard let response = response, let data = data else {
                return
            }
            print(response)

            do {
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                print(json)
            } catch {
                print(error)
            }
        }.resume()
    }
    
    static func fetchData(url: String, completion: @escaping (_ courses: [Course])->()) {
        
        guard let url = URL(string: url) else {
            return
        }
        URLSession.shared.dataTask(with: url) { (data, _, error) in
            
            guard let data = data else {
                return
            }
            
            do {
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let courses = try decoder.decode([Course].self, from: data)
                completion(courses)
                
            } catch let error {
                print("Error serialization json", error)
            }
        }.resume()
    }
    
    static func uploadImages(url: String) {
        
        let image = UIImage(named: "Lake")!
        let httpHeaders = ["Authorization":"_______"]
        guard let imageProperties = UploadImage(withImage: image, forKey: "image") else {
            return
        }
        guard let url = URL(string: url) else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = httpHeaders
        request.httpBody = imageProperties.data
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let response = response {
                print(response)
            }
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: [])
                    print(json)
                } catch {
                    print(error)
                }
            }
        }.resume()
    }
}
